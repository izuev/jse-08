package ru.tsc.izuev.tm.util;

import java.text.DecimalFormat;

public interface FormatUtil {

    final long KILOBYTE = 1024;

    final long MEGABYTE = KILOBYTE * 1024;

    final long GIGABYTE = MEGABYTE * 1024;

    final long TERABYTE = GIGABYTE * 1024;

    final DecimalFormat FORMAT = new DecimalFormat("#.###");

    static String formatBytes(long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) {
            return bytes + " B";
        } else if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) {
            return FORMAT.format((float) bytes / KILOBYTE) + " KB";
        } else if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) {
            return FORMAT.format((float) bytes / MEGABYTE) + " MB";
        } else if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) {
            return FORMAT.format((float) bytes / GIGABYTE) + " GB";
        } else if (bytes >= TERABYTE) {
            return FORMAT.format((float) bytes / TERABYTE) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}
